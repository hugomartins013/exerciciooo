package Exercicio2;

public class Losango extends Retangulo{
	
	private double diagonalA;
    private double diagonalB;
    
    public Losango(double diagonalA, double diagonalB, double ladoA, double ladoB, double ladoC, double ladoD) {
        super(ladoA, ladoB, ladoC, ladoD);
        this.diagonalA = diagonalA;
        this.diagonalB = diagonalB;
    }
	public double getDiagonalA() {
		return diagonalA;
	}
	public void setDiagonalA(double diagonalA) {
		this.diagonalA = diagonalA;
	}
	public double getDiagonalB() {
		return diagonalB;
	}
	public void setDiagonalB(double diagonalB) {
		this.diagonalB = diagonalB;
	}
    
	public void losango(){
        diagonalA = diagonalB;
    }

}
