package Exercicio2;
import static java.lang.Math.sqrt;

public class TrianguloRetangulo extends Triangulo{
	
	public TrianguloRetangulo(double lado1, double lado2, double lado3) {
        super(lado1, lado2, lado3);
    }
	
	public double retangulo(){
        double catetoA = super.getLadoB();
        double catetoB = super.getLadoC();
        double hipotenusa = sqrt((catetoA*catetoA)+(catetoB*catetoB));
        super.setLadoA(hipotenusa);
        return hipotenusa;
    }

}
