package Exercicio2;

public class Retangulo {
	
	private double ladoA;
    private double ladoB;
    private double ladoC;
    private double ladoD;
    
    public Retangulo(double ladoA, double ladoB, double ladoC, double ladoD) {
        this.ladoA = ladoA;
        this.ladoB = ladoB;
        this.ladoC = ladoC;
        this.ladoD = ladoD;
    }
	public double getLadoA() {
		return ladoA;
	}
	public void setLadoA(double ladoA) {
		this.ladoA = ladoA;
	}
	public double getLadoB() {
		return ladoB;
	}
	public void setLadoB(double ladoB) {
		this.ladoB = ladoB;
	}
	public double getLadoC() {
		return ladoC;
	}
	public void setLadoC(double ladoC) {
		this.ladoC = ladoC;
	}
	public double getLadoD() {
		return ladoD;
	}
	public void setLadoD(double ladoD) {
		this.ladoD = ladoD;
	}
    
	public void retangulo(){
        ladoA = ladoC;
        ladoB = ladoD;
    }
    

}
