package Exercicio1;

import java.util.ArrayList;

public class AnimalInvertebrado extends Animal{
	
	public AnimalInvertebrado(ArrayList<String> exoEsqueleto, boolean vida) {
        super(raca, vida);
        this.exoEsqueleto = exoEsqueleto;
       
    }
	private ArrayList<String> exoEsqueleto;

	public ArrayList<String> getExoEsqueleto() {
		return exoEsqueleto;
	}

	public void setExoEsqueleto(ArrayList<String> exoEsqueleto) {
		this.exoEsqueleto = exoEsqueleto;
	}
	
	public void moverExoEsqueleto(){
        vida = true;
    }
	
	
}
