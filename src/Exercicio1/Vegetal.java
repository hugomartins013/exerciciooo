package Exercicio1;

public class Vegetal extends SerVivo{
	public Vegetal(String tipo, boolean vida) {
        super(vida);
        this.tipo = tipo;
    }
	
	private String tipo;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public void fotossintese(){
        vida = true;
    }
}
