package Exercicio1;

public class Animal extends SerVivo{
	public Animal(String raca, boolean vida) {
        super(vida);
        this.raca = raca;
    }
	
	protected static String raca;

	public String getRaca() {
		return raca;
	}

	public void setRaca(String raca) {
		this.raca = raca;
	}
	
	public void movimentar(){
        vida = true;
    }

}
