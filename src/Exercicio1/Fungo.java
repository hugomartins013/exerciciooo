package Exercicio1;

public class Fungo extends SerVivo{
	
	public Fungo(String grupo, boolean vida) {
        super(vida);
        this.grupo= grupo;
    }
	
	public String grupo;

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	
	public void biorremediar(){
        vida = true;
    }
	

}
