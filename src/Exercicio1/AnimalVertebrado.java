package Exercicio1;

import java.util.ArrayList;

public class AnimalVertebrado extends Animal{
	
	public AnimalVertebrado(ArrayList<String> listaDeOssos, boolean vida) {
        super(raca, vida);
        this.listaDeOssos = listaDeOssos;
       
    }
	
	private ArrayList<String> listaDeOssos;

	public ArrayList<String> getListaDeOssos() {
		return listaDeOssos;
	}

	public void setListaDeOssos(ArrayList<String> listaDeOssos) {
		this.listaDeOssos = listaDeOssos;
	}
	
	public void moverEsqueleto(){
        vida = true;
    }
	

}
