package Exercicio1;

public class Monera extends SerVivo{
	public Monera(String classificacao, boolean vida) {
        super(vida);
        this.classificacao= classificacao;
    }
	
	public String classificacao;

	public String getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(String classificacao) {
		this.classificacao = classificacao;
	}
	
	public void infectar(){
        vida = true;
    }
	public void produzirProteina(){
        vida = true;
    }

}
