package Exercicio1;

public class Protista extends SerVivo{
	public Protista(String especie, boolean vida) {
        super(vida);
        this.especie= especie;
    }
	public String especie;

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}
	
	public void fotossintese(){
        vida = true;
    }
	

}
