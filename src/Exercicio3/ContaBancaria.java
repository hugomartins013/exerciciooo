package Exercicio3;

public class ContaBancaria {
	
	private String conta;
    private String agencia;
    private Cliente dadosPessoais;
    private double saldo;
    
    public ContaBancaria(String conta, Cliente dadosPessoais) {
        this.conta = conta;
        this.dadosPessoais = dadosPessoais;
    }
    
	public String getConta() {
		return conta;
	}
	public void setConta(String conta) {
		this.conta = conta;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public Cliente getDadosPessoais() {
		return dadosPessoais;
	}
	public void setDadosPessoais(Cliente dadosPessoais) {
		this.dadosPessoais = dadosPessoais;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
    
    

}
